//
//  MessagesFetcher.swift
//  SimpleMessenger
//
//  Created by Artur Davidian on 15/08/2019.
//  Copyright © 2019 SimpleMessengerLLC. All rights reserved.
//

import UIKit

struct FetchMessagesParam {
    var index = 0
    var limit = 0
}

struct FetchMessagesResult {
    var param: FetchMessagesParam
    var messages: [Message]
}

class Message {
    var text: String
    var date: Date
    
    init(text: String, date: Date) {
        self.text = text
        self.date = date
    }
}

protocol MessagesFetcherDelegate: class {
    func fetchingCompleted(_ fetcher: MessagesFetcher, with result: FetchMessagesResult)
    func fetchingFailed(_ fetcher: MessagesFetcher, with error: Error)
}

class MessagesFetcher {
    
    weak var delegate: MessagesFetcherDelegate?
    private(set) public var isBusy = false
    
    func fetchMessages(_ param: FetchMessagesParam) {
        
        isBusy = true
        let urlString = "http://134.209.252.144/messages?offset=\(param.index)&limit=\(param.limit)"
        guard let url = URL.init(string: urlString) else { return }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            var messages = [Message]()
            if let data = data {
                var json: Any?
                do {
                    json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                } catch let error {
                    DispatchQueue.main.async { [weak self] in
                        if let self = self {
                            self.isBusy = false
                            self.delegate?.fetchingFailed(self, with: error)
                        }
                    }
                }
                if let json = json as? [String: Any],
                    let items = json["data"] as? [Any] {
                    for item in items {
                        if let itemDict = item as? [String: Any],
                            let text = itemDict["msg"] as? String {
                            
                            let message = Message.init(text: text, date: Date.init())
                            messages.append(message)
                        }
                    }
                    
                    let result = FetchMessagesResult.init(param: param,
                                                          messages: messages)
                    
                    DispatchQueue.main.async { [weak self] in
                        if let self = self {
                            self.isBusy = false
                            self.delegate?.fetchingCompleted(self, with: result)
                        }
                    }

                }
                
            }
        }
        
        task.resume()
    }

}
