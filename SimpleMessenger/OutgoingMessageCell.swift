//
//  OutgoingMessageCell.swift
//  SimpleMessenger
//
//  Created by Artur on 15/08/2019.
//  Copyright © 2019 SimpleMessengerLLC. All rights reserved.
//

import UIKit

class OutgoingMessageCell: MessageCell {

    static let cellId = "OutgoingMessageCell"
    
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupBubbleView(bubbleView)
    }

    override func setup(with item: MessageItem) {
        super.setup(with: item)
        
        messageLabel.text = item.text ?? " "
        dateLabel.text = "00:00"
    }
    
}
