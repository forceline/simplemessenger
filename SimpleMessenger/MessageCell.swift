//
//  MessageCell.swift
//  SimpleMessenger
//
//  Created by Artur on 15/08/2019.
//  Copyright © 2019 SimpleMessengerLLC. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    private(set) public var messageItem: MessageItem!
    
    func setupBubbleView(_ aView: UIView) {
        aView.layer.cornerRadius = 16
    }
    
    func setup(with item: MessageItem) {
        self.messageItem = item
    }
    
}
