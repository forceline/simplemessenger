//
//  MessagesViewController.swift
//  SimpleMessenger
//
//  Created by Artur Davidian on 15/08/2019.
//  Copyright © 2019 SimpleMessengerLLC. All rights reserved.
//

import UIKit

class MessagesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var messageItems = [MessageItem]()
    private var presenter: MessagesPresenter

    // MARK: - UIViewController
    
    required init?(coder aDecoder: NSCoder) {
        presenter = MessagesPresenter()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.attachView(view: self)
        presenter.loadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        presenter.detachView()
        
        super.viewDidDisappear(animated)
    }

    // MARK: - Utility
    
    private func setupView() {
        messageItems.removeAll()
        
        tableView.delegate           = self
        tableView.dataSource         = self
        
        tableView.tableFooterView    = UIView.init(frame: .zero)
        tableView.rowHeight          = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
    }
}

// MARK: - MessagesMVPView

extension MessagesViewController: MessagesMVPView {
    
    func appendMessages(_ items: [MessageItem]) {
        messageItems.append(contentsOf: items)
        tableView.reloadData()
    }
    
}

// MARK: - UITableViewDataSource

extension MessagesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var result: UITableViewCell!
        let item = messageItems[indexPath.row]
        let isEvenIndex = (indexPath.row % 2 == 0)
        if isEvenIndex {
            if let cell = tableView.dequeueReusableCell(withIdentifier: OutgoingMessageCell.cellId, for: indexPath) as? OutgoingMessageCell {
                cell.setup(with: item)
                result = cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: IncomingMessageCell.cellId, for: indexPath) as? IncomingMessageCell {
                cell.setup(with: item)
                result = cell
            }
        }
        if result == nil {
            fatalError("table view cell not found")
        }
        return result
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let maxIndex = messageItems.count - 1
        if maxIndex - indexPath.row < MessagesPresenter.minCountToFetch {
            let startIndex = maxIndex + 1
            presenter.loadData(startIndex)
        }
    }
    
}

// MARK: - UITableViewDelegate

extension MessagesViewController: UITableViewDelegate {
    
}
