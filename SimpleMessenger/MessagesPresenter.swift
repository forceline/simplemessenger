//
//  MessagesPresenter.swift
//  SimpleMessenger
//
//  Created by Artur Davidian on 15/08/2019.
//  Copyright © 2019 SimpleMessengerLLC. All rights reserved.
//

import Foundation

struct MessageItem {
    var text: String?
    var date: Date?
}

protocol MessagesMVPView: class {
    func appendMessages(_ items: [MessageItem])
}

class MessagesPresenter: MessagesFetcherDelegate {
    
    static let defaultFetchLimit = 40
    static let minCountToFetch = defaultFetchLimit / 4
    
    weak var view: MessagesMVPView?
    private var fetcher: MessagesFetcher
    
    init() {
        fetcher = MessagesFetcher.init()
        fetcher.delegate = self
    }
    
    func attachView(view: MessagesMVPView) {
        self.view = view
    }
    
    func detachView() {
        self.view = nil
    }
    
    func loadData(_ startIndex: Int? = nil) {
        
        if !(fetcher.isBusy) {
            let param = FetchMessagesParam.init(index: startIndex ?? 0,
                                                limit: MessagesPresenter.defaultFetchLimit)
            fetcher.fetchMessages(param)
        }
        
    }

    // MARK: - MessagesFetcherDelegate
    
    func fetchingCompleted(_ fetcher: MessagesFetcher, with result: FetchMessagesResult) {
        let items      = result.messages.map{ MessageItem.init(text: $0.text, date: $0.date) }
        view?.appendMessages(items)
    }
    
    func fetchingFailed(_ fetcher: MessagesFetcher, with error: Error) {
        print(error)
    }
    
}
